<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    use HasFactory;
    protected $fillable = [
        'complaint_date',
        'content',
        'photo',
        'status',
        'user_id',
        'created_at',
        'updates_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function  responses()
    {
        return $this->hasMany(Response::class);
    }
    

}
  
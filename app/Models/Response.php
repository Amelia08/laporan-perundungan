<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    use HasFactory;
    protected $fillable = [
        'response_date',
        'response',
        'complaint_id',
        'operator_id',
        'created_at',
        'updates_at'
    ];
    public function complaint()
    {
        return $this->belongsTo(Complaint::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    

}

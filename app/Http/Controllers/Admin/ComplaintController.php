<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Complaint;
use App\Models\User;
use Illuminate\Database\QueryException;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $complaints = Complaint::paginate(5);
        return view('admin.complaints.index', ['complaint_list' => $complaints]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('admin.complaints.create', ['user_list' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'user_id' => 'required',
            'operator_id' => 'required',
            'complaint_date' => 'required',
            'content' => 'required',
            'photo' => 'nullable|file',
            'status' => 'required',
        ]);

        if (array_key_exists('photo', $data)) {
            $path = $request->photo->store('public/images');
            $data['photo'] = str_replace('public/', '', $path);
        }

        Complaint::create($data);

        return redirect('/admin/complaints');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $complaint = Complaint::find($id);
        return view('admin.complaints.detail', ['complaint' => $complaint]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $data = $request->validate([
               
               'status' => 'required',              
               'photo' => 'file',
               
            ]);

            Complaint::where('id', $id)->update($data);
    
            return redirect('/admin/complaints');
            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        {
            try {
                User::destroy($id);
                return redirect('/admin/complaints');
            } catch (QueryException $exc) {
                return redirect('/admin/complaints')
                    ->withErrors([
                        'msg' => 'User' . $id . 'cannot be deleted because related with other entity'
                    ]);
            }
        }
    }
}

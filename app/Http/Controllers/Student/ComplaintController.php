<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Complaint;
use App\Models\Student;
use Illuminate\Support\Facades\Auth;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $complaints = $user->complaints;

        return view('student.complaints.index', ['complaint_list' => $complaints]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Student::all();
        return view('student.complaints.create', ['student_list' => $students]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'complaint_date' => 'required',
            'content' => 'required',
            'photo' => 'nullable|file',
        ]);

 
        if (array_key_exists('photo', $data)) {
            $path = $request->photo->store('public/images');
            $data['photo'] = str_replace('public/', '', $path);
        }

        $data['user_id'] = Auth::user()->id;
       

        Complaint::create($data);

        return redirect('/student/complaints');
    }
   
   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $complaint = Complaint::find($id);

        $user = Auth::user();
        if ($complaint->user->id != $user->id) {
            return redirect('/student/complaints');
        }
        $complaint = Complaint::find($id);
        return view('student.complaints.detail', ['complaint' => $complaint]);
    }

    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return redirect('/student/complaints');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

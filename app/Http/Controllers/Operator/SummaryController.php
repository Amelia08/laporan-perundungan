<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Complaint;
use App\Models\Response;
use Illuminate\Http\Request;

class SummaryController extends Controller
{
    public function responseReport(Request $request)
    {
        $data = $request->validate([
            'start' => 'filled',
            'end' => 'filled'
        ]);
        
        if (array_key_exists('start', $data) && array_key_exists('end', $data)) {
            $responses = Response::where('response_date', '>=', $data['start'])
                ->where('response_date', '<=', $data['end'])->get();
            $start = $data['start'];
            $end = $data['end'];    
        } else {
            $responses = Response::all();
            $start = '';
            $end = '';
        }

        return view('operator.responses.summary', [
            'response_list' => $responses, 'start' => $start, 'end' => $end
        ]);
    }
}


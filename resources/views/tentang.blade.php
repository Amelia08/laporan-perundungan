@extends('app')

@section('content')

    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-3">
                    <img src="/img/buli5.jpg" alt="lt=image" height="200" width="260">
                </div>

                <div class="col-6">
                    <div class="perundungan">
                        <p style="font-family: Poppins;">Perundungan bisa dilakukan dimana saja, bahkan tanpa kita sadari.
                            Perundungan bisa berupa perundungan fisik, maupun verbal, dan juga bisa dilakukan di media
                            sosial. Kita bisa menghentikan bullying dengan hal-hal yang sederhana dan bisa dilakukan dalam
                            kehidupan sehari-hari. Orang tua, guru, siswa dapat menghentikan bullying dengan cara mengikuti
                            perkembangan informasi mengenai bullying, dan informasi mengenai pelaporan bullying.
                        </p>


                    </div>
                </div>
            </div>
        </div>
    </div>  
    
    
    
@endsection

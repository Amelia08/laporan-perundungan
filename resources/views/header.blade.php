<?php
$level = auth()->user()->level;
?>
<header class="ps-3 mb-5 border-bottom d-print-none" id="navigation-bar">
    <div class="container d-flex align-items-center">
        <a href="#" class="d-flex align-items-center ms-3 text-decoration-none">
            <img src="/img/nav.png" style="width: 200px">


        </a>
       
        <ul class="nav me-auto">
            <li><a href="/home" class="nav-link">Beranda</a></li>
            <li><a href="/bullying" class="nav-link">Apa itu bullying?</a></li>
            {{-- <li><a href="/tentang" class="nav-link">Tentang kami</a></li> --}}
            <li class="nav-item dropdown">
                @if ($level == 'student')
                    <a class="nav-link dropdown-toggle" href="/{{ $level }}/complaints/create" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        Lapor
                @endif
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <li><a href="http://localhost:8000/student/complaints/create" class="dropdown-item">Membuat laporan</a></li>
                    <li><a href="/{{ $level }}/complaints" class="dropdown-item">Laporan saya</a></li>
                    <li><a href="/{{ $level }}/responses" class="dropdown-item">Tanggapan</a></li>
                </ul>
            </li>
        </ul>
        {{-- <ul class="nav me-auto">
            <li><a href="/home" class="nav-link">Beranda</a></li>
            <li><a href="/bullying" class="nav-link">Apa itu bullying?</a></li>
            <li><a href="/tentang" class="nav-link">Tentang kami</a></li>
            @if ($level == 'student')
                <li><a href="/{{ $level }}/complaints/create" class="nav-link">Lapor</a></li>
            @endif


        </ul>
        <ul class="nav flex-coloumn mb-auto">
            @if ($level != 'student')
                <li><a href="/{{ $level }}/users" class="nav-link">User</a></li>
                <li><a href="/{{ $level }}/students" class="nav-link">Student</a></li>
            @endif

        </ul> --}}

        <div class="drodown ms-auto">
            <a href="#" class="dropdown-toggle text-decoration-none" data-bs-toggle="dropdown">
                <i class="bi bi-person-circle" style="font-size: 30px"></i>
            </a>
            <ul class="dropdown-menu">
                <li><a href="#" class="dropdown-item">Hai!, {{ auth()->user()->username }}</a></li>
                <li>
                    <hr class="dropdown-divider">
                </li>
                {{-- <li><a href="/student/complaints" class="dropdown-item">Laporan saya</a></li>
                <li>
                    <hr class="dropdown-divider">
                </li>
                <li><a href="/{{ $level }}/complaints" class="dropdown-item">Complaint</a></li>
                <li>
                    <hr class="dropdown-divider">
                </li>
                <li><a href="/{{ $level }}/responses" class="dropdown-item">Response</a></li>
                <li>
                    <hr class="dropdown-divider">
                </li> --}}
                <li><a href="/logout" class="dropdown-item">Log out</a></li>
                <li>
                    <hr class="dropdown-divider">
                </li>
               
            </ul>
        </div>
    </div>
</header>

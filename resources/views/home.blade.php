@extends('app')

@section('content')
<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
      {{-- <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button> --}}
    </div>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/img/bg3.png" alt="image" height="650" width="1800">
        
      </div>
      <div class="carousel-item">
        <img src="/img/Home4.png"alt="image" height="650" width="1800">
       
      </div>
      {{-- <div class="carousel-item">
        <img src="/img/student.png" alt="image" height="650" width="1800">  
      </div> --}}
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  </div>

    
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-6 mt-5 mb-5">
                    <p class="font-t"></p>
                    <p class="display-6" id="home-text">Matthew 22:39 The second is like it: ‘Love your neighbor as
                        yourself.</p>
                    <div class="col-6 mt-3 mb-5" id="button">
                        <a href="/student/complaints/create">
                            <button type="button" class="btn btn-success">Sampaikan laporan anda!</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container ms-2">
        <div class="row" id="stop">
            <div class="col-6" id="stop-img">
                <img src="/img/bg5.png" alt="lt=image" height="500" width="1200">
            </div>
        </div>
    </div>

    <div class="container ms-1">
        <div class="row" id="bully">
            <div class="col-6" id="bully-img">
                <img src="/img/home.png" alt="lt=image" height="600" width="1650">
            </div>
        </div>
    </div>
    <div class="grid text-center">
        <div class="row">
            <div class="col-6">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/86_uuX77hsc " id="video-bully"
                    title="YouTube video player" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    allowfullscreen></iframe>
            </div>
            <div class="col-6">
                <h2 style="font-family: Poppins; text-align:left" id="edukasi-dukungan">Dengan cinta, dukungan, dan
                    kepedulian, kita dapat membantu mencegah perundungan dan melindungi kesejahteraan
                    lingkungan
                    sekolah demi masa depan yang baik.</h2>
                </div>
        </div>
    </div>

    {{-- <div class="container" id="edukasi">
        <div class="row">
            <div class="col-6" id="edukasi-youtube">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/86_uuX77hsc " id="video-bully"
                    title="YouTube video player" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    allowfullscreen></iframe>
            </div>

            <div class="col-6 ">
                <h1 class="display-4" id="edukasi-kata">Edukasi Bullying</h1>
                <br>

                <h2 style="font-family: Poppins;" id="edukasi-dukungan">Dengan cinta, dukungan, dan
                    kepedulian, kita dapat membantu mencegah perundungan dan melindungi kesejahteraan
                    lingkungan
                    sekolah demi masa depan yang baik.</h2>
                <a href="https://youtu.be/86_uuX77hsc">
                    <button type="button" class="btn btn-success py-2 px-3" style="border-radius: 0;"
                        id="edukasi-video">Tonton
                        Video
                        Selengkapnya
                    </button>
                </a>

            </div>
        </div> --}}
@endsection

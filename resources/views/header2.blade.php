<?php
$level = auth()->user()->level;
?>

<header class="ps-3 mb-5 border-bottom d-print-none" id="navigation-bar">
    <div class="container d-flex align-items-center">
        <a href="#" class="d-flex align-items-center me-3 text-decoration-none">
            <img src="/img/nav.png" style="width: 200px">


        </a>

        <ul class="nav mb-auto">
            @if ($level != 'student')
                <li><a href="/{{ $level }}/users" class="nav-link">User</a></li>
                <li><a href="/{{ $level }}/students" class="nav-link">Student</a></li>
            @endif
            <li><a href="/{{ $level }}/complaints" class="nav-link">Complaint</a></li>
            <li><a href="/{{ $level }}/responses" class="nav-link">Response</a></li>
            @if ($level != 'student')
                <li><a href="/{{ $level }}/response-complaint" class="nav-link">Generate Laporan</a></li>
            @endif
        </ul>

        <div class="dropdown ms-auto">
            <a href="#" class="dropdown-toggle text-decoration-none" data-bs-toggle="dropdown">
                <i class="bi bi-person-circle" style="font-size: 35px"></i>
            </a>
            <ul class="dropdown-menu">
                <li><a href="#" class="dropdown-item">Hai!, {{ auth()->user()->username }}</a></li>
                <li>
                    <hr class="dropdown-divider">
                </li>

                <li><a href="/logout" class="dropdown-item">Log out</a></li>
            </ul>
        </div>
    </div>

</header>

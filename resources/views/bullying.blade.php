@extends('app')

@section('content')
    {{-- <div class="container">
        <div id="hal-bully"> --}}
            {{-- <div class="row"> --}}
                {{-- <div class="col-6" id="video-bullying">
                    <iframe width="850" height="450" src="https://www.youtube.com/embed/Rhinz16z7tM"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                        allowfullscreen></iframe>
                </div>

                <div class="container ms-2">
                    <div class="row bullying">
                        <div class="col-6" id="bullying-img">
                            <img src="/img/bully.png" alt="lt=image" height="500" width="1200">
                        </div>
                    </div> --}}
                {{-- </div> --}}
                {{-- <div class="col-3 mt-5" id="apa-bullying">
                    <h2 class="display-4" style="font-size: 50">Apa itu Bullying?</h2>
                    <br>
                </div>

                <div class="col-6 ms-2" id="text-berita">
                    <h2 id="text-bully">Banyak orang sudah mengetahui bullying, tapi tidak semua
                        orang
                        tau jenis bullying dan bagaimana mencegahnya. Kita semua pasti pernah melakukan bullying
                        tanpa kita sadari, ada pepatah "lebih baik mencegah daripada mengobati" alangkah baiknya
                        kita mengenal bullying dan belajar mencegah bullying sejak dini dengan melakukan hal
                        simple
                        sebelum lebih banyak lagi korban bullying. </h2>

                    <h2 id="text-artikel">Meski bisa terjadi pada semua usia,
                        bullying paling sering
                        terjadi saat anak berada di fase remaja.
                        Perilaku ini biasanya dilakukan oleh individu atau kelompok yang lebih kuat melakukan
                        penindasan kepada yang lebih lemah
                        Perlu diketahui bahwa perundungan bukan hanya menyerang fisik tetapi juga kejiwaan atau
                        mental seseorang</h2>
                </div> --}}
               
            {{-- </div> --}}
        {{-- </div> --}}

        {{-- <div class="container text-center">
            <div class="row">
                <div class="col align-self-start">
                    <iframe width="400" height="350" src="https://www.youtube.com/embed/Rhinz16z7tM"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                        allowfullscreen></iframe>
                    <h1>Verbal Bullying</h1>
                    <p  style="text-align: left">Banyak pelaku perundungan verbal ini mengatakan bahwa mereka hanya sedang bercanda saja dan menyebut korban dengan kata "baperan" jika merasa tersinggung dengan kalimat atau perkataan tidak menyenangkan yang mereka ucapkan. Perundungan verbal atau verbal bullying biasanya berupa kalimat kasar atau ejekan yang ditunjukan pada seseorang.
                        Dampak verbal bullying adalah anak atau siswa menjadi takut berbicara atau mengemukakan pendapat. Korban perundungan verbal/ verbal bullying memiliki  ketakutan ketika harus tampil di muka umum karena trauma pada tanggapan atau ucapan buruk yang pernah diterimanya. Meskipun sering diremehkan, ternyata perundungan verbal memiliki efeknya jangka panjang dan sangat membekas pada korbannya
                        
                    </p>
                </div>
                <div class="col align-self-center">
                    <iframe width="400" height="350" src="https://www.youtube.com/embed/Rhinz16z7tM"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                        allowfullscreen></iframe>
                    <h2>Pyhsical Bullying</h2>
                    <p style="text-align: left">Bullying fisik ini dilakukan secara langsung yang dapat meninggalkan bekas yang mudah terlihat oleh semua orang, berbeda dengan verbal bullying. 
                        Orang yang melakukan Physical bullying ini melakukan kekerasan kepada anggota tubuh korban. Contoh bullying fisik yaitu memukul, mendorong, menjambak, 
                        menendang, menampar, mengunci seseorang dalam ruangan, mencubit, mencekik, menggigit, mencakar, meludahi dan merusak serta menghancurkan barang-barang milik anak yang tertindas, memeras, dan lain-lainnya. 
                        Sehingga korban menimbulkan rasa takut yang bedara dan meninggalkan jejak trauma. Bahkan Physical bullying ini dapat menyebabkan korban meninggal dunia.      
                    </p>
                </div>
                <div class="col align-self-start">
                    <iframe width="400" height="350" src="https://www.youtube.com/embed/Rhinz16z7tM"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                        allowfullscreen></iframe>
                    <h1>Verbal Bullying</h1>
                    <p  style="text-align: left">Banyak pelaku perundungan verbal ini mengatakan bahwa mereka hanya sedang bercanda saja dan menyebut korban dengan kata "baperan" jika merasa tersinggung dengan kalimat atau perkataan tidak menyenangkan yang mereka ucapkan. Perundungan verbal atau verbal bullying biasanya berupa kalimat kasar atau ejekan yang ditunjukan pada seseorang.
                        Dampak verbal bullying adalah anak atau siswa menjadi takut berbicara atau mengemukakan pendapat. Korban perundungan verbal/ verbal bullying memiliki  ketakutan ketika harus tampil di muka umum karena trauma pada tanggapan atau ucapan buruk yang pernah diterimanya. Meskipun sering diremehkan, ternyata perundungan verbal memiliki efeknya jangka panjang dan sangat membekas pada korbannya
                        
                    </p>
                </div>
                <div class="col align-self-center">
                    <iframe width="400" height="350" src="https://www.youtube.com/embed/Rhinz16z7tM"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                        allowfullscreen></iframe>
                    <h2>Pyhsical Bullying</h2>
                    <p style="text-align: left">Bullying fisik ini dilakukan secara langsung yang dapat meninggalkan bekas yang mudah terlihat oleh semua orang, berbeda dengan verbal bullying. 
                        Orang yang melakukan Physical bullying ini melakukan kekerasan kepada anggota tubuh korban. Contoh bullying fisik yaitu memukul, mendorong, menjambak, 
                        menendang, menampar, mengunci seseorang dalam ruangan, mencubit, mencekik, menggigit, mencakar, meludahi dan merusak serta menghancurkan barang-barang milik anak yang tertindas, memeras, dan lain-lainnya. 
                        Sehingga korban menimbulkan rasa takut yang bedara dan meninggalkan jejak trauma. Bahkan Physical bullying ini dapat menyebabkan korban meninggal dunia.      
                    </p>
                </div>
                
                <div class="col align-self-end">
                    <iframe width="400" height="350" src="https://www.youtube.com/embed/Rhinz16z7tM"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                        allowfullscreen ></iframe>
                    <h2>Bullying social</h2>
                    <p style="text-align: left">Selain Bullying verbal dan physical aja juga jenis perundungan yang sering terjadi yaitu pengucilan.
                        Korban tidak disakiti secara fisik maupun verbal, tetapi dimusuhi dan diabaikan oleh lingkungan
                        pergaulannya.
                        Dampak yang terjadi dapat menyebabkan anak akan kesulitan mencari teman, karena biasanya pelaku
                        punya pengaruh yang cukup kuat untuk membujuk orang lain mengucilkan korban.</p>
                </div>
            </div>
        </div> --}}
    {{-- </div> --}}


    <div class="row row-cols-1 row-cols-md-2 g-4">
        <div class="col" style="width: 600px; height: 500px; margin-left: 250px">
          <div class="card" style="background-color: #FFC93C">
            <img src="/img/verbal2.png" class="card-img-top" >
            <div class="card-body">
              <h5 class="card-title" style="color: white; font-family: Lobster; font-size: 50px" >VERBAL BULLYING</h5>
              <p class="card-text" style="color: white; font-size: 20px">Verbal bullying adalah jenis kekerasan  yang dilakukan melalui kata-kata, seperti mengolok-olok, mencaci-maki, mempermalukann, atau mengejek seseorang dengan maksud untuk menyakiti perasaannya. Banyak pelaku perundangan verbal ini mengatakan bahwa mereka hanya sedang bercanda.  Hal ini dapat dilakukan langsung melalui percakapan tatap muka atau melalui media sosial atau pesan teks.<br>
                Contoh verbal bullying adalah saat seseorang secara terus-menerus mengatakan hal-hal buruk tentang penampilan fisik seseorang, mengolok-olok cara berbicara seseorang, atau mengatakan hal-hal yang mempermalukan seseorang di depan orang lain. Hal-hal ini dapat menyebabkan  seseorang menjadi tidak percaya diri.</p>
            </div>
          </div>
        </div>
        <div class="col" style="width: 600px; height: 500px">
          <div class="card" style="background-color: #DC3535">
            <img src="/img/berantem.png" class="card-img-top" style="height: 450px; width: 450px">
            <div class="card-body">
              <h5 class="card-title" style="color: white; font-family: Lobster; font-size: 50px; margin-top: 80px">PHYSICAL BULLYING</h5>
              <p class="card-text" style="color: white; font-size: 20px">Physical bullying adalah tindakan kekerasan yang melibatkan tindakan fisik, seperti meninju, menendang,memukul, mendorong, menjambak,. Bullying fisik ini dilakukan secara langsung yang dapat meninggalkan bekas yang mudah terlihat oleh semua orang, berbeda dengan verbal bullying yang melibatkan penggunaan kata-kata yang menyakiti.
            physical bullying bisa dilakukan oleh satu orang atau  pun secara berkelompok, para pelaku memilih  seseorang  yang lebih lemah atau lebih kecil dari dirinya. kekerasan ini dapat menimbulkan rasa sakit atau cedera pada korban baik secara kesehatan fisik ataupun mental mereka.
                <p class="card-text" style="color: white; font-size: 20px">Bullying bisa terjadi karena ada nya penyebab, tetapi bullying ini tidak bisa dibenarkan karena dapat merugikan orang lain. 
                Pelaku bullying ini bisa ada karena tidak dapat mengontrol diri, ataupun emosi.</p>
            </div>
          </div>
        </div>
        <div class="col" style="width: 600px; height: 500px; margin-top: 700px; margin-left: 250px">
          <div class="card" style="background-color: #F7C8E0">
            <img src="/img/social.png" class="card-img-top" >
            <div class="card-body">
              <h5 class="card-title"  style="color: white; font-family: Lobster; font-size: 50px">SOCIAL BULLYING</h5>
              <p class="card-text"  style="color: white; font-size: 20px">Social bullying atau bullying sosial adalah tindakan yang dilakukan dengan tujuan untuk membuat seseorang merasa tertekan, malu, dan dijauhi dari lingkungan sosialnya. Bentuk bullying ini tidak memakai kekerasan seperti physical bullying tetapi tetapi lebih bersifat sosial, seperti menyebarkan rumor yang belum pasti kebenarannya untuk menjauhi seseorang, menghindari seseorang, mengabaikan dan lain-lainnya.</p>
                
            </div>
          </div>
        </div>
        <div class="col"style="width: 600px; height: 500px; margin-top: 700px">
          <div class="card"  style="background-color: #5BC0F8">
            <img src="/img/cyber2.png" class="card-img-top" style="height: 315px; width: 450px">
            <div class="card-body">
              <h5 class="card-title"  style="color: white; font-family: Lobster; font-size: 50px">CYBERBULLYING</h5>
              <p class="card-text"  style="color: white; font-size: 20px">Cyber bullying adalah tindakan mengganggu, menakut-nakuti, atau merendahkan seseorang secara online atau melalui teknologi digital seperti media sosial.  Cyber bullying bisa dilakukan secara sekelompok orang misalnya membuat grup untuk menjelek-jelekkan seseorang dan ini bisa terjadi secara terus-menerus dalam jangka waktu yang lama.  Selain itu mengirimkan pesan yang tidak sopan atau mengancam, memposting foto atau video yang mempermalukan, atau membuat akun palsu untuk membuat berita palsu adalah contoh dari cyberbullying. </p>
            </div>
          </div>
        </div>
      </div>
       
@endsection

{{-- @extends('app')

@section('content')
<div class="carousel-item active">
    <img src="/img/student.png" alt="image" height="650" width="1705">
</div>
@endsection --}}

@extends('app')

@section('content')
    <div class="halaman-lapor"></div>
    <div class="container">
        <div class="row">
            <h1 style="font-family: Russo One; color: #7c7d81; margin-left: 260px">Laporan Pengaduan</h1>
            <div class="col-6 mx-auto" id="hal-lapor">
                <form action="/student/complaints" method="POST" enctype="multipart/form-data"
                    style="height: 370px; width: 600px">
                    @csrf
                    <div class="row flex-coloumn">
                        <div class="col-12 mb-3">
                            <label for="complaint_date" class="form-label">Complaint_date</label>
                            <input type="date" class="form-control" id="complaint_date" name="complaint_date">
                        </div>
                    </div>
                    <div class="row flex-coloumn">
                        <div class="col-12 mb-3">
                            <label for="content" class="form-label"> Content</label>
                            <input type="text" class="form-control" id="content" name="content" style="height: 200px">
                        </div>
                    </div>
                    <div class="row flex-coloumn">
                        <div class="col-12 mb-3">
                            <label for="photo" class="form-label">Bukti</label>
                            <input type="file" class="form-control" id="photo" name="photo"
                                accept="image/png,image/jpeg">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="reset" class="btn btn-primary">Reset</button>
                </form>
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <p class="text-danger">{{ $error }}</p>
                    @endforeach
                @endif
            </div>
        </div>

    </div>
@endsection

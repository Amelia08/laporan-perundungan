@extends('app')

@section('content')
    <div class="container">
        <h1>Detail Response</h1>
        <form action="/student/responses/{{ $response->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="operator_id" class="form-label">Operator id</label>
                    <input type="text" class="form-control" id="operator_id" name="operator_id"
                        value="{{ $response->operator_id }}" disabled>
                </div>
            </div>
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="complaint" class="form-label">Content</label>
                    <input type="date" class="form-control" id="complaint" name="complaint"
                        value="{{ $response->response_date }}" disabled>
                </div>
            </div>
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="response_date" class="form-label">Response Date</label>
                    <input type="date" class="form-control" id="response_date" name="response_date"
                        value="{{ $response->response_date }}" disabled>
                </div>
            </div>
            <div class="row flex-coloumn">
                <div class="col-15 mb-3">
                    <label for="response" class="form-label">Response</label>
                    <input type="text" class="form-control" id="response" name="response"
                        value="{{ $response->response }}" disabled>
                </div>
            </div>

        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection

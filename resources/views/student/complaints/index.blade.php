@extends('app')

@section('content')
    <div class="container">
        <h1>Pengaduan</h1>
        <table class="table">
            <thead>
                <tr>

                    <th>complaint_date</th>
                    <th>content</th>
                    <th>photo</th>
                    <th>status</th>
                    <th>created_at</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($complaint_list as $complaint)
                    <tr>

                        <td>{{ $complaint->complaint_date }}</td>
                        <td>{{ $complaint->content }}</td>
                        <td>{{ $complaint->photo }}</td>
                        <td>{{ $complaint->status }}</td>
                        <td>{{ $complaint->created_at }}</td>

                        <td> 
                        <a href="/student/complaints/{{ $complaint->id }}" class="btn btn-danger">Detail</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="/student/complaints/create" class="btn btn-outline-danger">Create</a>
    </div>
@endsection

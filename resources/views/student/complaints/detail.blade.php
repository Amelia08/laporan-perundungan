@extends('app')

@section('content')
    <div class="container">
        <h1>Detail Complaint</h1>
        <form action="/student/complaints/{{ $complaint->id }}" method="POST">
            @csrf
            @method('PATCH')

            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="complaint_date" class="form-label">Complaint_date</label>
                    <input type="date" class="form-control" id="complaint_date" name=" complaint_date"
                        value="{{ $complaint->complaint_date }}" disabled>
                </div>
            </div>
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="content" class="form-label"> Content</label>
                    <input type="text" class="form-control" id="content" name=" content"
                        value="{{ $complaint->content }}" disabled>
                </div>
            </div>
            <div class="row flex-coloumn">
                <input type="file" class="form-control" id="photo" name="photo" accept="image/png,image/jpeg" disabled>
                <img src="{{ asset('storage/' . $complaint->photo) }}" style="width: 300px">
            </div>

        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>

    
@endsection

@extends('app2')

@section('content')
    <div class="container">
        <h1>Detail Complaint</h1>
        <form action="/operator/complaints/{{ $complaint->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="user_id" class="form-label">User id</label>
                    <input type="text" class="form-control" id="user_id" name="user_id" value="{{ $complaint->user_id }}" disabled>
                </div>
            </div>
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" class="form-control" id="name" name="name"
                        value="{{ $complaint->user->name }}" disabled>
                </div>
            </div>
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="complaint_date" class="form-label">Complaint_date</label>
                    <input type="date" class="form-control" id="complaint_date" name="complaint_date"
                        value="{{ $complaint->complaint_date }}" disabled>
                </div>
            </div>
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="content" class="form-label"> Content</label>
                    <input type="text" class="form-control" id="content" name="content"
                        value="{{ $complaint->content }}" disabled>
                </div>
            </div>

            <div class="col-3 mb-3">
                <label for="photo" class="form-label">Bukti</label>
                <input type="file" class="form-control" id="photo" name="photo" 
                      accept="image/png,image/jpeg">
                <img src="{{ asset('storage/' . $complaint->photo) }}" style="width: 300px">
            </div>


            <div class="col-3 mb-3">
                <label class="form-label">Status</label>
                <select name="status" class="form-select">
                    @foreach (['new', 'verified', 'reject', 'done'] as $item)
                        <option value="{{ $item }}" {{ $complaint->level == $item ? 'selected' : '' }}>
                            {{ $item }}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-primary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection

@extends('app2')

@section('content')
    <div class="container">
        <h1>Pengaduan</h1>
        <p>{{ $complaint_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>user ID</th>
                    <th>name</th>
                    <th>report date</th>
                    <th>content</th>
                    <th>photo</th>
                    <th>status</th>
                    <th>action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($complaint_list as $complaint)
                    <tr>
                        <td>{{ $complaint->user_id }}</td>
                        <td>{{ $complaint->user->name}}</td>
                        <td>{{ $complaint->complaint_date }}</td>
                        <td>{{ $complaint->content }}</td>
                        <td>{{ $complaint->photo }}</td>
                        <td>{{ $complaint->status }}</td>
                        <td>
                            <a href="/operator/complaints/{{ $complaint->id }}" class="btn btn-danger">Detail</a>
                           

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="/operator/complaints/create" class="btn btn-outline-danger">Create</a>

    </div>
@endsection

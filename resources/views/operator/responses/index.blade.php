@extends('app2')

@section('content')
    <div class="container">
        <h1>Tanggapan</h1>
        <p>{{ $response_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>Complaint ID</th>
                    <th>Content</th>
                    <th>Operator ID</th>
                    <th>Rsesponse_date</th>
                    <th>Response</th>
                    
                    <th>action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($response_list as $response)
                    <tr>
                        <td>{{ $response->complaint_id }}</td>
                        <td>{{ $response->complaint->content }}</td>
                        <td>{{ $response->operator_id }}</td>
                        <td>{{ $response->response_date }}</td>
                        <td>{{ $response->response }}</td>
                       <td>
                            <a href="/operator/responses/{{ $response->id }}" class="btn btn-warning">Detail</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="/operator/responses/create" class="btn btn-outline-warning">Create</a>
    </div>
@endsection

@extends('app2')

@section('content')
    <div class="container" id="user-home">
        <h1>Data Siswa</h1>
        <p>{{ $user_list->links() }}</p>
        <table class="table" id="table-text">
                <thead>
                    <tr>
                        <th>name</th>
                        <th>username</th>
                        <th>email</th>
                        <th>phone</th>
                        <th>address</th>
                        <th>level</th>
                        <th>action</th>


                    </tr>
                </thead>
                <tbody>
                    @foreach ($user_list as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->phone }}</td>
                            <td>{{ $user->address }}</td>
                            <td>{{ $user->level }}</td>

                            <td>
                                <a href="/operator/users/{{ $user->id }}" class="btn btn-info">Detail</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
           
        </table>
        <a href="/operator/users/create" class="btn btn-outline-info">Create</a>
    </div>
@endsection

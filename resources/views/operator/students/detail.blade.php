@extends('app2')

@section('content')
    <div class="container">
        <h1>Detail student</h1>
        <form action="/operator/students/{{ $student->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="nisn" class="form-label">Nisn</label>
                    <input type="text" class="form-control" id="nisn" name="nisn" value="{{ $student->nisn }}" disabled>
                </div>
            </div>
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $student->user->name }}" disabled>
                </div>
            </div>
             <div class="col-3 mb-3">
                <label class="form-label">Class</label>
                <select name="class" class="form-select">
                    @foreach (['7','8','9','10', '11', '12'] as $item)
                        <option value="{{ $item }}">
                            {{ $item }}</option>
                    @endforeach
                </select>
            </div>
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="user_id" class="form-label">User </label>
                    <input type="text" class="form-control" id="user_id" name="user_id" value="{{ $student->user_id }}">
                </div>
            </div>
            


           
            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-primary">Reset</button>
        </form>
        @if ($errors->any())
        @foreach ($errors->all() as $error)
            <p class="text-danger">{{ $error }}</p>
        @endforeach
    @endif
    </div>
@endsection

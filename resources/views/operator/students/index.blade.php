@extends('app2')

@section('content')
    <div class="container">
        <h1>Data Siswa</h1>
        <p>{{ $student_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>user_id</th>
                    <th>name</th>
                    <th>nisn</th>
                    <th>class</th>
                    <th>level</th>
                    <th>action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($student_list as $student)
                    <tr>
                        <td>{{ $student->user_id }}</td>
                        <td>{{ $student->user->name }}</td>
                        <td>{{ $student->nisn }}</td>
                        <td>{{ $student->class }}</td>
                        <td>{{ $student->user->level }}</td>
                        
                        <td>
                            <a href="/operator/students/{{ $student->id }}" class="btn btn-success">Detail</a>

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="/operator/students/create" class="btn btn-outline-success">Create</a>
    </div>
@endsection

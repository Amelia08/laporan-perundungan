@extends('app2')

@section('content')
    <div class="container">
        <h1>Data Siswa</h1>
        <p>{{ $student_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>

                    <th>user_id</th>
                    <th>name</th>
                    <th>nisn</th>
                    <th>class</th>
                    <th>level</th>
                    <th>action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($student_list as $student)
                    <tr>

                        <td>{{ $student->user_id }}</td>
                        <td>{{ $student->user->name }}</td>
                        <td>{{ $student->nisn }}</td>
                        <td>{{ $student->class }}</td>
                        <td>{{ $student->user->level }}</td>

                        <td>
                            <a href="/admin/students/{{ $student->id }}" class="btn btn-success">Detail</a>
                            <a href="#" class="btn btn-danger" data-bs-toggle="modal"
                                data-bs-target="#modal-{{ $student->id }}">Delete</a>
                        </td>
                        </td>
                    </tr>
                    <div class="modal fade" id="modal-{{ $student->id }}" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Konfirmasi</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div>
                                <div class="modal-body">
                                    <p>student dengan ID {{ $student->id }} akan dihapus.</p>
                                    <p>Lanjutkan?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="/admin/students/{{ $student->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Batal</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        <a href="/admin/students/create" class="btn btn-outline-danger">Create</a>
        @if ($errors->any())
        @foreach ($errors->all() as $error)
            <p class="text-danger">{{ $error }}</p>
        @endforeach
    @endif
    </div>
@endsection

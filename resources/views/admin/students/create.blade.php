@extends('app2')

@section('content')
    <div class="container">
        <h1>Create student</h1>
        <form action="/admin/students" method="POST">
            @csrf
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
            </div>
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="nisn" class="form-label">Nisn</label>
                    <input type="text" class="form-control" id="nisn" name="nisn">
                </div>
            </div>
            <div class="col-3 mb-3">
                <label class="form-label">Class</label>
                <select name="class" class="form-select">
                    @foreach (['7','8','9','10', '11', '12'] as $item)
                        <option value="{{ $item }}">
                            {{ $item }}</option>
                    @endforeach
                </select>
            </div>
           
           
            <div class="col-3 mb-3">
                <label class="form-label">Gender</label>
                <select name="gender" class="form-select">
                    @foreach (['female','male'] as $item)
                        <option value="{{ $item }}">
                            {{ $item }}</option>
                    @endforeach
                </select>
            </div>
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="date_of_birth" class="form-label">Date of Birth</label>
                    <input type="date" class="form-control" id="date_of_birth" name="date_of_birth">
                </div>
            </div>
            
            
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="username" class="form-label">Username </label>
                    <input type="text" class="form-control" id="username" name="username">
                </div>
            </div>
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="password" class="form-label">Password </label>
                    <input type="text" class="form-control" id="password" name="password">
                </div>
            </div>
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="email" email="form-label">Email </label>
                    <input type="text" class="form-control" id="email" name="email">
                </div>
            </div>
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="phone" email="form-label">Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone">
                </div>
            </div>
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="address" email="form-label">Address</label>
                    <input type="text" class="form-control" id="address" name="address">
                </div>
            </div>
           

            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-primary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection

@extends('app2')

@section('content')
    <div class="container">
        <h1>Tanggapan</h1>
        <p>{{ $response_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>complaint Id</th>
                    <th>content</th>
                    <th>operator_id</th>
                    <th>response_date</th>
                    <th>response</th>
                    <th>action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($response_list as $response)
                    <tr>
                        <td>{{ $response->complaint_id }}</td>
                        <td>{{ $response->complaint->content }}</td>
                        <td>{{ $response->operator_id }}</td>
                        <td>{{ $response->response_date }}</td>
                        <td>{{ $response->response }}</td>
                        <td>
                            <a href="/admin/responses/{{ $response->id }}" class="btn btn-primary">Detail</a>
                            <a href="#" class="btn btn-warning" data-bs-toggle="modal"
                                data-bs-target="#modal-{{ $response->id }}">Delete</a>
                        </td>
                        </td>
                    </tr>
                @endforeach
            </tbody>


            <div class="modal fade" id="modal-{{ $response->id }}" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Konfirmasi</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                        </div>
                        <div class="modal-body">
                            <p>Response dengan ID {{ $response->id }} akan dihapus.</p>
                            <p>Lanjutkan?</p>
                        </div>
                        <div class="modal-footer">
                            <form action="/admin/responses/{{ $response->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Hapus</button>
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </table>
        <a href="/admin/responses/create" class="btn btn-outline-primary">Create</a>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection

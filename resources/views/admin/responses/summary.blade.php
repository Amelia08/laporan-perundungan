@extends('app2')

@section('content')
    <div class="container ms-5" >
        <h1>Generate Laporan</h1>
        <div class="row mb-2">
            <div class="col-md-4 ml-4">
                <form action="/admin/response-complaint" method="GET">
                    <div class="form-group">
                        <input type="text" name="keyword" id="search" class="form-control" placeholder="Cari nama pelapor">
                    </div>
            </div>
            <div class="col-md-2">
                <button type="submit" id="filter" class="btn btn-primary">Cari</button>
                </form>
            </div>
        </div>
        <form action="/admin/response-complaint" method="GET">

            <div class="row">
                <label for="start" class="col-1 col-form-label">Dari</label>
                <div class="col-2">
                    <input type="date" class="form-control" id="start" name="start" value="{{ $start }}">
                </div>
                <label for="end" class="col-1 col-form-label">Sampai</label>
                <div class="col-2">
                    <input type="date" class="form-control" id="end" name="end" value="{{ $end }}">
                </div>
                <div class="col-2">
                    <button type="submit" class="btn btn-success">Cari</button>
                </div>
            </div>
        
        </form>
        <table class="table">
            <thead>
                <tr>
                    <th>user_id</th>
                    <th>name</th>
                    <th>complaint_date</th>
                    <th>content</th>
                    <th>photo</th>
                    <th>status</th>
                    <th>operator_id</th>
                    <th>response_date</th>
                    <th>response</th>


                </tr>
            </thead>
            <tbody>
                @foreach ($response_list as $response)
                    <tr>
                        <td>{{ $response->complaint->user_id }}</td>
                        <td>{{ $response->complaint->user->name }}</td>
                        <td>{{ $response->complaint->complaint_date }}</td>
                        <td>{{ $response->complaint->content }}</td>
                        <td>{{ $response->complaint->photo }}</td>
                        <td>{{ $response->complaint->status }}</td>
                        <td>{{ $response->complaint->operator_id }}</td>
                        <td>{{ $response->response_date }}</td>
                        <td>{{ $response->response }}</td>

                    </tr>
                @endforeach
            </tbody>
        </table>
        <button type="button" class="btn btn-secondary" onclick="window.print()">
            Print
        </button>
    </div>
@endsection

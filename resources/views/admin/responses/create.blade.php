@extends('app2')

@section('content')
    <div class="container">
        <h1>Detail Response</h1>
        <form action="/admin/responses" method="POST">
            @csrf
            <div class="col-3 mb-3">
                <label for="response_date" class="form-label">Response date</label>
                <input type="date" class="form-control" id="response_date" name="response_date">
            </div>


            <div class="col-3 mb-3">
                <label for="response" class="form-label">Response </label>
                <input type="text" class="form-control" id="response" name="response">
            </div>


            <div class="col-3 mb-3">
                <label class="form-label">Complaint ID</label>
                <select name="complaint_id" class="form-select">
                    @foreach ($complaint_list as $complaint)
                        <option value="{{ $complaint->id }}">{{ $complaint->id }} - {{ $complaint->content }}</option>
                    @endforeach
                </select>
            </div>


            <div class="col-3 mb-3">
                <label class="form-label">Operator Id</label>
                <select name="operator_id" class="form-select">
                    @foreach ($user_list as $user)
                        <option value="{{ $user->id }}">{{ $user->id }} - {{ $user->operator }}</option>
                    @endforeach
                </select>
            </div>




            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-primary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection

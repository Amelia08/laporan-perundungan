@extends('app2')

@section('content')
    <div class="container">
        <h1>Laporan Pengaduan</h1>
        <form action="/admin/complaints" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label class="form-label">User id</label>
                    <select name="user_id" id="user_id">
                        @foreach ($user_list as $user)
                            <option value="{{ $user->id }}">{{ $user->id }} - {{ $user->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-3 mb-3">
                <label class="form-label">Operator Id</label>
                <select name="operator_id" class="form-select">
                    @foreach ($user_list as $user)
                        <option value="{{ $user->id }}">{{ $user->id }} - {{ $user->user_id }}</option>
                    @endforeach
                </select>
            </div>



            <div class="col-3 mb-3">
                <label for="complaint_date" class="form-label">Complaint_date</label>
                <input type="date" class="form-control" id="complaint_date" name="complaint_date">
            </div>

            <div class="col-3 mb-3">
                <label for="content" class="form-label"> Content</label>
                <input type="text" class="form-control" id="content" name="content">
            </div>

            <div class="col-3 mb-3">
                <label for="photo" class="form-label">Bukti</label>
                <input type="file" class="form-control" id="photo" name="photo" accept="image/png,image/jpeg">
            </div>


            <div class="col-3 mb-3">
                <label class="form-label">Status</label>
                <select name="status" class="form-select">
                    @foreach (['new', 'verified', 'reject', 'done'] as $item)
                        <option value="{{ $item }}">
                            {{ $item }}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-primary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection

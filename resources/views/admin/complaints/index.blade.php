@extends('app2')

@section('content')
    <div class="container">
        <h1>Pengaduan</h1>
        <p>{{ $complaint_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>user ID</th>
                    <th>name</th>
                    <th>Report Date</th>
                    <th>content</th>
                    <th>photo</th>
                    <th>status</th>
                    <th>action</th>
                   
                </tr>
            </thead>
            <tbody>
                @foreach ($complaint_list as $complaint)
                    <tr>
                        <td>{{ $complaint->user_id}}</td>
                        <td>{{ $complaint->user->name}}</td>
                        <td>{{ $complaint->complaint_date }}</td>
                        <td>{{ $complaint->content }}</td>
                        <td>{{ $complaint->photo}}</td>
                        <td>{{ $complaint->status }}</td>
                        <td>
                            <a href="/admin/complaints/{{ $complaint->id }}" class="btn btn-warning">Detail</a>
                            <a href="#" class="btn btn-outline-danger" data-bs-toggle="modal"
                                data-bs-target="#modal-{{ $complaint->id }}">Delete</a>
                        </td>
                    </tr>

                    <div class="modal fade" id="modal-{{ $complaint->id }}" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Konfirmasi</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div>
                                <div class="modal-body">
                                    <p>complaint dengan ID {{ $complaint->id }} akan dihapus.</p>
                                    <p>Lanjutkan?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="/admin/complaints/{{ $complaint->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Batal</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="modal-{{ $complaint->id }}" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Konfirmasi</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div>
                                <div class="modal-body">
                                    <p>Complaint dengan ID {{ $complaint->id }} akan dihapus.</p>
                                    <p>Lanjutkan?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="/admin/complaints/{{ $complaint->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Batal</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        <a href="/admin/complaints/create" class="btn btn-outline-warning">Create</a>
        @if ($errors->any())
        @foreach ($errors->all() as $error)
            <p class="text-danger">{{ $error }}</p>
        @endforeach
    @endif
    </div>
@endsection

<footer class="site-footer">
    <div class="container-fluid d-print-none">
        <div class="row">
            <div class="col-sm-12 col-md-6" style="text-align: left">
                <a href="#" class="d-flex align-items-center ms-3 text-decoration-none">
                    <img src="/img/nav.png" style="width: 350px">
                </a>
            </div>

            <div class="col-sm-5 col-md-3" style="text-align: left">
                <h6>Kontak</h6>
                <ul class="footer-links">
                    <p class="bi bi-envelope-at-fill"> smpgracia@gmail.com</p>
                    <p class="bi bi-telephone-fill"> 0895356012314 </p>
                </ul>
            </div>

            <div class="col-xs-5 col-md-3" style="text-align: left">
                <h6>Menu</h6>
                <ul class="footer-links">
                    <li><a href="http://localhost:8000/home">Beranda</a></li>
                    <li><a href="http://localhost:8000/bullying">Apa itu Bullying?</a></li>

                </ul>
            </div>
            <hr>
            <p class="copyright-text">Copyright 2023 by Gracia
            </p>
        </div>
       
    </div>
    {{-- <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p class="copyright-text">Copyright 2023 by Gracia
                </p>
                <div class="footer-icons" style="margin-bottom: 10px margin-right: 65px display-flex">
                    <a href="#"><i class="fa fa-facebook" id="fb"></i></a>
                    <a href="#"><i class="fa fa-twitter" id="tw"></i></a>
                    <a href="#"><i class="bi bi-instagram" id="ig"></i></a>
                    <a href="#"><i class="fa fa-linkedin" id="in"></i></a>
            </div>
        </div>
    </div> --}}
</footer>

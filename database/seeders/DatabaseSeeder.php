<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Student;
use App\Models\Complaint;
use App\Models\Response;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user_1 = User::create([
            'name' => 'agustinus maruli tua',
            'gender' => 'male',
            'date_of_birth' => '2005-07-01',
            'username' => 'agus_grc1',
            'password' => Hash::make('Coba_123'),
            'email' => 'agus11@gmail.com',
            'phone' => '081220636437',
            'address' => 'Jalan Sukamulya no.13',
            'level' => 'student'
        ]);
        $user_2 = User::create([
            'name' => 'grace kristiani',
            'gender' => 'female',
            'date_of_birth' => '2005-12-26',
            'username' => 'grace_grc8',
            'password' => Hash::make('manis_223'),
            'email' => 'krsitianigrace@gmail.com',
            'phone' => '081220637878',
            'address' => 'Jalan rajawali timur no.13',
            'level' => 'student'
        ]);
        $user_3 = User::create([
            'name' => 'amelia venesa',
            'gender' => 'female',
            'date_of_birth' => '2005-06-05',
            'username' => 'amelia_grc2',
            'password' => Hash::make('Coba_08605'),
            'email' => 'ameliavenesa0806@gmail.com',
            'phone' => '0895356012314',
            'address' => 'Jalan cibangkong n0.90',
            'level' => 'admin'
        ]);
        $user_4 = User::create([
            'name' => 'christopher rafael gunawan',
            'gender' => 'male',
            'date_of_birth' => '2003-11-02',
            'username' => 'christopher_grc5',
            'password' => Hash::make('ayo_123'),
            'email' => 'crafaelgunawan@gmail.com',
            'phone' => '08122065432',
            'address' => 'Jalan saturnus n0.13',
            'level' => 'student'
        ]);
        $user_5 = User::create([
            'name' => 'reymond gunadi',
            'gender' => 'male',
            'date_of_birth' => '2005-05-30',
            'username' => 'reymond_op17',
            'password' => Hash::make('Coba_349'),
            'email' => 'agustinus11@gmail.com',
            'phone' => '081920906437',
            'address' => 'Jalan pagarsih no.34',
            'level' => 'operator'
        ]);
        $user_6 = User::create([
            'name' => 'marsella natalia',
            'gender' => 'female',
            'date_of_birth' => '2004-12-26',
            'username' => 'marsella_adm15',
            'password' => Hash::make('kue_863'),
            'email' => 'marsella12@gmail.com',
            'phone' => '0812210101',
            'address' => 'Jalan padalarang no.113',
            'level' => 'admin'
        ]);

        $student_1 = Student::create([
            'nisn' => '0043699212',
            'class' => '12',
            'user_id' => $user_1->id
        ]);
        $student_2 = Student::create([
            'nisn' => '006779920',
            'class' => '12',
            'user_id' => $user_2->id
        ]);
        $student_3 = Student::create([
            'nisn' => '0067799216',
            'class' => '12',
            'user_id' => $user_3->id
        ]);
        $student_4 = Student::create([
            'nisn' => '0067799213',
            'class' => '12',
            'user_id' => $user_4->id
        ]);
        $complaint_1 = Complaint::create([
            'complaint_date' => '2022-12-12',
            'content' => 'teman-teman saya mengambil bekal makanan saya',
            'photo' => 'aaa',
            'status' => 'new',
            'user_id' => $user_1->id,
        ]);
        $complaint_2 = Complaint::create([
            'complaint_date' => '2023-01-22',
            'content' => 'saya mengalami perundungan ketika pulang sekolah, saya dicegat oleh adik kelas',
            'photo' => 'bukti',
            'status' => 'verified',
            'user_id' => $user_2->id,
        ]);
        $complaint_3 = Complaint::create([
            'complaint_date' => '2023-02-01',
            'content' => 'saya mengalami perundungan ketika pulang sekolah, saya dicegat oleh adik kelas',
            'photo' => 'bukti',
            'status' => 'verified',
            'user_id' => $user_3->id,
        ]);
        $complaint_4 = Complaint::create([
            'complaint_date' => '2023-01-08',
            'content' => 'saya mengalami perundungan ketika pulang sekolah, saya dicegat oleh adik kelas',
            'photo' => 'bukti',
            'status' => 'verified',
            'user_id' => $user_4->id,
        ]);
        $complaint_5 = Complaint::create([
            'complaint_date' => '2023-01-08',
            'content' => 'saya mengalami perundungan ketika pulang sekolah, saya dicegat oleh adik kelas',
            'photo' => 'bukti',
            'status' => 'verified',
            'user_id' => $user_5->id,

        ]);
        $response_1 = Response::create([
            'complaint_id' => $complaint_1->id,
            'response_date' => '2022-12-22',
            'response' => 'Terima kasih sudah melapor, kami akan memberikan sanksi kepada pelaku',
            'operator_id' => $user_3->id
        ]);      
        $response_2 = Response::create([
            'response_date' => '2022-02-02',
            'response' => 'Terima kasih sudah melapor, kami akan memberikan sanksi kepada pelaku',
            'complaint_id' => $complaint_2->id,
            'operator_id' => $user_3->id
        ]);
        $response_3 = Response::create([
            'response_date' => '2023-02-05',
            'response' => 'Terima kasih sudah melapor, kami akan memberikan sanksi kepada pelaku',
            'complaint_id' => $complaint_3->id,
            'operator_id' => $user_4->id
        ]);
        $response_4 = Response::create([
            'response_date' => '2023-01-10',
            'response' => 'Terima kasih sudah melapor, kami akan memberikan sanksi kepada pelaku',
            'complaint_id' => $complaint_4->id,
            'operator_id' => $user_5->id
        ]);
        $response_5 = Response::create([
            'response_date' => '2023-01-10',
            'response' => 'Terima kasih sudah melapor, kami akan memberikan sanksi kepada pelaku',
            'complaint_id' => $complaint_5->id,
            'operator_id' => $user_5->id
        ]);

    }
}
